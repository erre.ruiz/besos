import subprocess
import os

SCRIPT_NAME = "generate_environments.sh"

basic = {"."}
complete = {".[complete]"}
test = basic | {
    "coveralls",
    "sklearn",
    "Pulp<=2.1",
    "pytest",
    "pytest-cov",
    "pytest-mock",
    "pytest-regtest",
    "pyKriging",
}
dev = {"black", "pre-commit", "sphinx"} | complete | test

environments = {"basic": basic, "complete": complete, "test": test, "dev": dev}

for env_name, requirements in environments.items():
    print(f"making {env_name}")
    with open(SCRIPT_NAME, "w") as f:
        lines = [
            "#!/bin/bash",
            "rm -rf temp_env",  # ignore failures if the directory already exists
            "python -m venv temp_env",
            ". temp_env/bin/activate",
            "pip install wheel",
            # add quotes around each requirement to support pinned versions
            " ".join(["pip", "install"] + [f'"{req}"' for req in requirements]),
            f"pip freeze > environments/requirements-{env_name}.txt",
            "deactivate",
        ]
        for line in lines:
            f.write(line + "\n")
    os.chmod(SCRIPT_NAME, mode=0o770)
    command = [f"./{SCRIPT_NAME}"]
    result = subprocess.run(command, capture_output=True)
    if result.stderr:
        print(result.stdout, result.stderr)
        raise subprocess.CalledProcessError(result.returncode, command)

subprocess.run("rm -rf temp_env".split())
subprocess.run(["rm", SCRIPT_NAME])
subprocess.run("mv environments/requirements-basic.txt ./requirements.txt".split())
