---
title: 'BESOS: Building and Energy Simulation, Optimization and Surrogate-modelling'
tags:
  - Python
  - building energy simulation
  - machine learning
  - optimization
  - surrogate modelling
authors:
  - name: Paul Westermann
    orcid: 0000-0002-3676-5211
    affiliation: "1,2" # (Multiple affiliations must be quoted)
  - name: Theodor Victor Christiaanse
    orcid: 0000-0003-4082-3338
    affiliation: "1,2"
  - name: Will Beckett
    affiliation: 1
  - name: Paul Kovacs
    affiliation: 1
  - name: Ralph Evins^[Corresponding author]
    orcid: 0000-0003-4963-5798
    affiliation: "1,2"
affiliations:
 - name: Energy in Cities group, Department of Civil Engineering, University of Victoria, British Columbia, Canada
   index: 1
 - name: Institute for Integrated Energy Systems, University of Victoria, British Columbia, Canada
   index: 2
date: 13 June 2020
bibliography: paper.bib

---
# Summary
The buildings sector is one of the largest contributors to carbon emissions, up to 33% of global CO2 emissions [@urge-vorsatzMitigatingCO2Emissions2007]. Improved computational methods are needed to help design more energy-efficient buildings. BESOS is a Python library and associated web-based platform to help researchers and practitioners to explore energy use in buildings more effectively. This is achieved by providing an easy way of integrating many disparate aspects of building modelling, district modelling, optimization and machine learning into one common library.

![analysis domains encompassed by BESOS.\label{fig:example}](images/besos.png)

# Statement of Need
As in many domains, computational tools for exploring building energy use are currently fragmented, requiring users to manually integrate many different tools, each with their own installation and configuration issues. This is particularly arduous when using machine-learning models together with building energy models, each requiring extensive domain knowledge to implement from scratch. To alleviate this, BESOS implements the following approaches to the modelling and optimization of buildings in one common library.

- A parametric interface to the popular building simulation software EnergyPlus (https://energyplus.net/) [@crawley2001energyplus], built on EPPy (https://pypi.org/project/eppy/).
- District-scale operational and capacity optimization using the Energy Hub (https://pypi.org/project/pyehub/) [@evins2014new] model.
- Large-scale parametric analyses with easy results exploration (using Pandas DataFrames and standard plotting libraries).
- Machine learning based surrogate models that approximate detailed physics-based simulations [@westermann2019surrogate] using scikit-learn [@scikit-learn] and TensorFlow.
- A pipeline to run building design optimizations [@waibel2019building] using meta-heuristics like genetic algorithms (using Platypus), either over EnergyPlus directly or over a surrogate model.
- A set of data structures for handling the different parts of the approaches above: a Problem consisting of Parameters, and Objectives is associated with a building model, then sampled and passed to an Evaluator.

The first key contribution of BESOS is the ease with which the above features can be interlinked. For example, hourly energy demands for a year can be calculated for a set of buildings using a parameterized EnergyPlus simulation, then linked to an Energy Hub model to find the optimal operational and sizing of a district system. This coupled model can then be linked to a multi-objective genetic algorithm to find the trade-off between investment cost and carbon emissions by manipulating both building and district-level variables.

The second key contribution of BESOS is in facilitating surrogate modelling, where machine learning models are trained on many samples of simulation input and output data so that the resulting model can be used as a fast but approximate surrogate for the computationally-intensive simulation. This allows users to rapidly explore the entire 'design space' of possible buildings, rather than optimizing for pre-specified objectives. BESOS provides a complete set of scripts for deriving and using surrogate models of building energy simulations, including problem definition, sampling and batch processing of simulations, and machine learning model training and testing.

# Using the library via a web-interface: the BESOS Platform (https://besos.uvic.ca/)
The BESOS Platform [@faureBESOSCollaborativeBuilding2019] provides executable Jupyter Notebooks (https://jupyter.org/) via the Jupyter Lab interface that implement many of the features of the BESOS library in a web-interface with no set-up required. These notebooks mix executable code, visual outputs and formatted explanations, making them ideal for non-expert programmers who want to get on with running building energy models without navigating a huge codebase. Expert users have full control of their environment, with terminal access to pip install packages or to pull from git repositories.

The platform hosts a multitude of example notebooks (https://gitlab.com/energyincities/besos/-/tree/master/examples) which serve as tutorials, and can be copied and adapted to accomplish new tasks. These cover all BESOS functionality, like optimizing building designs, deriving surrogate models (inc. uncertainty analysis and sensitivity analysis), and running coupled EnergyPlus and Energy Hub models.

The platform is a JupyterHub environment that launches a new Jupyter Notebook server for each user, complete with the BESOS library and all dependencies (including EnergyPlus and Energy Hub) pre-installed alongside many other Python packages which are useful for conducting building energy analyses. All dependencies and packages are automatically kept up-to-date, with scheduled tests to ensure compatibility. Access is available to all for non-commercial use, with computing resources shared under a fair-use policy. Load balancing is handled by Docker Swarm. Apart from serving as cloud-computing resource to run the BESOS library, the platform also includes detailed instructions to outsource large-scale building simulation jobs and machine learning training jobs to external high-performance computing clusters.

# Associated research and projects
- The BESOS library is the foundation for the Net Zero Navigator (https://netzeronavigator.ca/) tool which provides interactive visual design-space exploration for net-zero ready buildings based on surrogate models.
- Surrogate modelling research using BESOS includes using convolutional neural networks (CNN) as climate-independent building energy surrogate models [@WESTERMANN2020115563], the use of active learning to derive surrogate models with fewer samples [@westermannAdaptiveSamplingBuilding], and the use of a surrogate model to analyze the key parameters of ground-source heat pumps [@WESTERMANN2020114715].
- Energy Hub applications of BESOS include storage sizing [Paper Azin] and PV potential analysis [Paper Theo].

# Acknowledgments
The development of BESOS was largely conducted by undergraduate students studying Computer Science or Software Engineering at the University of Victoria. Special thanks to coop programming team; Will Beckett, Mark Hills, Chase Xu, Dylan Kemp, Madhur Panwar, James Comish and Polina Chaikovsky. The work was funded by the CANARIE Research Software Program (https://www.canarie.ca/software/) (grant RS-327).

# References
