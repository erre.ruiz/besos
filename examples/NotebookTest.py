import os
import papermill as pm
from papermill.exceptions import PapermillExecutionError

target_directory = "./"

ipynb_list = []
for root, dirs, files in os.walk(target_directory):
    dirs[:] = [d for d in dirs if not d.startswith(".")]
    for file in files:
        if (
            file.endswith(".ipynb")
            and file != "NotebookTest.ipynb"
            and file != "DifferentVersionEP.ipynb"
            and file != "Workshop_RemoteCommunities.ipynb"
        ):
            ipynb_list.append((file, os.path.join(root, file)))

if ipynb_list == []:
    print(f"No notebook found under {target_directory}")
    raise FileNotFoundError

error_list = []

for ipynb in ipynb_list:
    try:
        pm.execute_notebook(ipynb[1], ipynb[1], cwd=ipynb[1].split(ipynb[0])[0])
    except PapermillExecutionError as e:
        print(e)
        error_list.append(ipynb[1])

if error_list != []:
    raise Exception(error_list)
