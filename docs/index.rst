*********************************
Welcome to besos' documentation!
*********************************

besos library and BESOS platform have been created by the Energy In Cities Group and Univeristiy of Victoria. The Platform is a Jupyter Hub that is able to run the besos code base, and has all the dependencies installed. The platform is freely accesible for academics in the building energy modelling space. 

If you want to get started on the BESOS platform, check out how to get started `here! <https://gitlab.com/energyincities/besos/blob/master/Getting%20Started%20on%20BESOS%20and%20making%20%20your%20first%20Notebook.pdf>`_

Besos library connects modelling and optimisation tools to allow for parameterizing
running and optimizing models, sampling from their design spaces, and generating data for use
in machine learning models. This supports designing building and district energy modelling experiments.

The first step is the creation of a parameterized model. This can use an
`EnergyPlus <https://energyplus.net>`_ model or a `PyEHub <https://python-ehub.readthedocs.io>`_ model.
Each parameter is setup to select what is to
be changed through `selectors <https://gitlab.com/energyincities/besos/-/blob/master/examples/Evaluators/Selectors.ipynb>`_
and a the specifying the domain of interest through `descriptors <https://gitlab.com/energyincities/besos/-/blob/master/examples/Evaluators/Descriptors.ipynb>`_. These are
combined to form the parameters of the model. The next step is determine the
outputs that we are interested in through objectives. These can be any of the
outputs provided by EnergyPlus or EnergyHub.


This parameterized model can be combined with a EnergyPlus or EnergyHub model, which can be optimized or sampled from.
`Evaluators <https://gitlab.com/energyincities/besos/-/blob/master/examples/Evaluators/Evaluators.ipynb>`_ handle
running the model to find a sampled dataset or allow a optimizer to find the best solution
given the problem definition. The sampled data can be used to train a
surrogate model or use other machine learning techniques to understand the design space.

Examples of applications of Besos to building and district energy modelling are available on our `examples overview page <https://gitlab.com/energyincities/besos/-/blob/master/examples/ExamplesOverview.ipynb>`_ or take a look at the example workflow that can be created in the left side bar. 

.. include:: file_blurbs.rst

.. toctree::
	:maxdepth: 2
	:caption: BESOS
	:glob:

	building_to_surrogate_tf.rst
	modules/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
