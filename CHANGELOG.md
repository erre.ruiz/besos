## Changelog

### [Development](https://gitlab.com/energyincities/besos/-/tree/dev)
### Removed
* [!90](https://gitlab.com/energyincities/besos/-/merge_requests/90) Removed `utils.py` and the contained functions `create_temp_directory` and `resolve_path`


### [Version 1.6.1](https://gitlab.com/energyincities/besos/-/releases#1.6.1)
#### Changed
* [!70](https://gitlab.com/energyincities/besos/-/merge_requests/70) Fix BESOS [readthedocs](https://besos.readthedocs.io/en/stable/) pages.  Stable = latests tagged release.  Latest = dev branch.

### [Version 1.6.0](https://gitlab.com/energyincities/besos/-/releases#1.6.0)
#### Added
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 `keep_dirs` flag to `EvaluatorEP.df_apply`. It will cause each run's output to be stored
 in it's own directory. This directory's name is stored in a results column.  
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 Evaluation functions for `EvaluatorGeneric` and `error_values` for all Evaluators can now represent their result as 
a single tuple of all the objective and constraint values.
*  [!49](https://gitlab.com/energyincities/besos/-/merge_requests/49)
 Buildings with `HVACTemplate` objects are now supported. They will
 have the `--expandoutputs` flag added when running, preventing them from erroring out.
*  [!48](https://gitlab.com/energyincities/besos/-/merge_requests/48)
 A previous removal of the `epw_file` argument is now backwards compatible. Warnings
 will be emitted instead of errors when `EvaluatorEP` is initialised with this argument.

#### Changed
*  [!47](https://gitlab.com/energyincities/besos/-/merge_requests/47)
 `ParameterEH` should be replaced by a `Parameter` and
 a `PathSelector`. Examples updated accordingly
*  [!44](https://gitlab.com/energyincities/besos/-/merge_requests/44)
 Update Selector Documentation.
* [!57](https://gitlab.com/energyincities/besos/-/merge_requests/57)
 `eppySupport.get_idfobject_from_name` has been moved to `eppy_funcs.get_idfobject_from_name`
*  [59!](https://gitlab.com/energyincities/besos/-/merge_requests/59)
 The `get_building` and `get_idf` functions will warn instead of erroring when an idd is already set
 and no idd is provided on a subsequent call. (The error was from trying to change the idd to the default
 value.) If a new idd is provided, the functions will still raise an error.

#### Deprecated
*  [!47](https://gitlab.com/energyincities/besos/-/merge_requests/47)
 `ParameterEH` should be replaced by a `Parameter` and a `PathSelector`
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 Evaluation functions for `EvaluatorGeneric` and `error_values` should use a single tuple of values rather than 
 separate tuples for outputs and constraints.
* [!57](https://gitlab.com/energyincities/besos/-/merge_requests/57)
 All functions from `eppySupport` are now deprecated.
* [!62](https://gitlab.com/energyincities/besos/-/merge_requests/62)
  The `version` argument for `objectives.read_eso` is deprecated, since
  the correct version is now extracted automatically.
* [67!](https://gitlab.com/energyincities/besos/-/merge_requests/67)
  The version argument on `EvaluatorEP.__init__`, `eplus_funcs.run_building`
  and `eplus_funcs.print_available_outputs` is deprecated, since
  the correct version is now extracted automatically.
* [67!](https://gitlab.com/energyincities/besos/-/merge_requests/67)
  The `eplus_funcs.check_idf_version` function is deprecated, since using
  we use the correct version number automatically.
  

### [Version 1.5.0](https://gitlab.com/energyincities/besos/-/releases#1.5.0)
#### Added
*  Path for Bonmin to the 'rbf_opt' function so that 'rbf_opt' can be configured for a cluster
*  Random seed number to the 'rbf_opt' function

----

### [Version 1.4.3](https://gitlab.com/energyincities/besos/-/releases#1.4.3)
#### Added
*  Additional examples
*  DASK framework to run E+ jobs in parallel
*  Integrated Geomeppy - geometric edits to E+ files
*  Generic evaluator class
*  Daylight control features
*  Black formatter standard
#### Changed
*  Updated examples
*  Improved error handling
*  Energy Plus default version to 9.1.0
*  Window to wall ratio function can now be set via direction

----

### [Version 1.3.3](https://gitlab.com/energyincities/besos/-/releases#1.3.3)
#### Added
*  Energy Hub parameter class - 'ParameterEH'
#### Changed
*  bug fix to adaptive sampling class

----

### [Version 1.3.1](https://gitlab.com/energyincities/besos/-/releases#1.3.1)
#### Changed
* Additional fix to error output handling

----

### [Version 1.3.0](https://gitlab.com/energyincities/besos/-/releases#1.3.0)
#### Changed
*  Major bug fix to prevent overwriting files, redirection of E+ output files.

----

### [Version 1.2.3](https://gitlab.com/energyincities/besos/-/releases#v1.2.3)
#### Added
*  'adaptive_sampler_lv' sampling class
*  Getting Started PDF document
#### Changed
*  Documentation changes on [readthedocs](https://besos.readthedocs.io/en/latest/#)

----

### [Version 1.2.0](https://gitlab.com/energyincities/besos/-/releases#v1.2.0)
#### Added
* 'full_factorial' sampling function

----

### [Version 1.1.0](https://gitlab.com/energyincities/besos/-/releases#v1.1.0)
#### Added
*  EnergyHub Evaluator - EvaluatorEH object
#### Changed
*  Improved testing parameters and scope
*  File path restructuring
*  Various bug fixes

----

### [Version 1.0.0](https://gitlab.com/energyincities/besos/-/releases#v1.0.0)
*  Initial release of the BESOS repo, view examples and [readthedocs](https://besos.readthedocs.io/) for more information and documentation
*  Available on PyPI (`pip install besos`)
